package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testConvertFromCelsius() {
		assertTrue("Invalid value for fahrenheit",41== Fahrenheit.convertFromCelsius(5));
	}

	@Test
	public void testIsNotConvertFromCelsius() {
		assertFalse("Invalid value for fahrenheit",30== Fahrenheit.convertFromCelsius(5));
	}
	@Test
	public void testBoundryInConvertFromCelsius() {
		assertTrue("Invalid value for fahrenheit", 32== Fahrenheit.convertFromCelsius(0));
	}
	@Test
	public void testBoundryOutConvertFromCelsius() {
		assertFalse("Invalid value for fahrenheit", 37.4== Fahrenheit.convertFromCelsius(3));
	}

}
