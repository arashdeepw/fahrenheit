package sheridan;

public class Fahrenheit {
	
	public static int convertFromCelsius(int cel) {
		int f = (int)Math.round((cel*1.8)+32);
		return f;
	}

	public static void main(String[] args) {
		System.out.println("Fahrenheit is: "+Fahrenheit.convertFromCelsius(5));

	}

}

